package main

import (
	"fmt"
)

func main() {
	var countrys, classmates int

	_, _ = fmt.Scan(&countrys)

	needIncome := make([]int, countrys)
	for i := 0; i < countrys; i++ {
		_, _ = fmt.Scan(&needIncome[i])
	}

	needEducation := make([]int, countrys)
	for i := 0; i < countrys; i++ {
		_, _ = fmt.Scan(&needEducation[i])
	}

	relatives := make([]int, countrys)
	for i := 0; i < countrys; i++ {
		_, _ = fmt.Scan(&relatives[i])
	}

	_, _ = fmt.Scan(&classmates)

	income := make([]int, classmates)
	for i := 0; i < classmates; i++ {
		_, _ = fmt.Scan(&income[i])
	}

	education := make([]int, classmates)
	for i := 0; i < classmates; i++ {
		_, _ = fmt.Scan(&education[i])
	}

	relative := make([]int, classmates)
	for i := 0; i < classmates; i++ {
		_, _ = fmt.Scan(&relative[i])
	}

	flag := make([]int, classmates)

	for i := 0; i < countrys; i++ {
		for j := 0; j < classmates; j++ {
			switch {
			case income[j] >= needIncome[i] && education[j] >= needEducation[i] && income[j] != 0:
				flag[j] = i + 1
				continue
			case relative[j] != 0 && relatives[relative[j]-1] == 1:
				flag[j] = i + 1
				break
			default:
				flag[j] = 0
			}

		}
	}
	for s := 0; s < classmates; s++ {
		fmt.Print(flag[s], " ")
	}
}
